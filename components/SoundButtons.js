import React, {useEffect, useState} from 'react';
import {StatusBar} from 'expo-status-bar';
import {StyleSheet, TouchableOpacity, Text, View, Button, ScrollView} from 'react-native';
// import { Icon } from 'react-native-elements';
import { Audio } from 'expo-av';
import SoundButton from "./SoundButton";
import FloatingActionButton from "./FloatingActionButton";

export default function SoundButtons({buttons}) {
    const [sound, setSound] = useState()

    useEffect(() => {
        return sound
            ? () => {
                sound.unloadAsync()
            }
            : undefined;
    }, [sound])

    /**
     * Play sound by color of button
     */
    const playSound = async (soundUri) => {
        const _sound = await Audio.Sound.createAsync({uri: soundUri});
        setSound(_sound.sound)
        await _sound.sound.playAsync()
    }

    return (
        <View>
            <ScrollView contentContainerStyle={styles.view}>
                {buttons.map(btn => (
                    <SoundButton key={btn.id} color={btn.color} label={btn.label} uri={btn.uri} onPress={() => {playSound(btn.uri)}}/>
                ))}
            </ScrollView>
        </View>
    );
}

const styles = StyleSheet.create({
    view: {
        // flex: 1,
        flexDirection: 'row',
        flexWrap: 'wrap',
        alignItems: 'center',
        justifyContent: 'center',
        backgroundColor: '#000',
        paddingTop: 60,
        paddingBottom: 30
    }
});
