import {StatusBar} from 'expo-status-bar';
import {StyleSheet, View} from 'react-native';
import SoundButtons from "./components/SoundButtons";
import FloatingActionButton from "./components/FloatingActionButton";
import {useState} from "react";
import AddButtonModal from "./components/AddButtonModal";

export default function App() {

    //TODO: Redux
    //TODO: Able Deleting btns
    //TODO: Comments
    //TODO: Correct delay before hearing a sound
    //TODO: Center btns
    //TODO: Feedback when recording
    //TODO: Feedback when playing soud
    //TODO: Stop playing sound when open modal

    const [isModalVisible, setIsModalVisible] = useState(false);
    const [soundButtons, setSoundButtons] = useState([
        {id: 1, color: '#0000ff', label: 'My cat yelling', uri: ''},
        {id: 2, color: '#0eaa03', label: 'Ok this is a test', uri: ''},
        {id: 3, color: '#ff00ae', label: 'Lorem ipsum blah', uri: ''},
        {id: 4, color: '#ffa600', label: 'A sound button', uri: ''},
        {id: 5, color: '#ff004d', label: 'My dog yelling', uri: ''},
        {id: 6, color: '#009e99', label: 'My sister laughing', uri: ''},
        {id: 7, color: '#00d9ff', label: 'see all these btns ?', uri: ''},
        {id: 8, color: '#8400ff', label: ':D', uri: ''},
        {id: 9, color: '#ff00d5', label: 'My mother yellin', uri: ''},
        {id: 10, color: '#99F', label: 'dfg dgdsgd', uri: ''},
        {id: 424240, color: '#1b2c6e', label: 'wonderful app', uri: ''},
        {id: 19559951, color: '#ffa600', label: 'a big fart', uri: ''},
        {id: 2724242424, color: '#be1379', label: 'TEST', uri: ''},
        {id: 114646, color: '#ad9202', label: 'dsdsgdg', uri: ''},
        {id: 1153535, color: '#ad9202', label: 'aaaaa', uri: ''},
        {id: 153351, color: '#41801d', label: 'TEST', uri: ''},
        {id: 11533, color: '#0000ff', label: 'ok sound', uri: ''},
        {id: 117575, color: '#009e99', label: 'TEST', uri: ''},
        {id: 1155463, color: '#00d9ff', label: 'TEST', uri: ''},
        {id: 1142424, color: '#ab2929', label: 'Weird sound', uri: ''},
        {id: 11538383, color: '#ad9202', label: 'TEST', uri: ''},
        {id: 11535353535, color: '#510e88', label: 'TEST', uri: ''},
        {id: 11767676, color: '#ad9202', label: 'TEST', uri: ''},
        {id: 1156565242494914, color: '#ad9202', label: 'TEST', uri: ''},
        {id: 16837763444461, color: '#ad9202', label: 'TEST', uri: ''},
        {id: 1683776461, color: '#ad9202', label: 'TEST', uri: ''},
        {id: 232425, color: '#ad9202', label: 'TEST', uri: ''},
        {id: 168336363634343776461, color: '#ad9202', label: 'TEST', uri: ''},
        {id: 168334434776461, color: '#ad9202', label: 'TEST', uri: ''},
        {id: 16343443776461, color: '#ff004d', label: 'TEST', uri: ''},
        {id: 333333333333, color: '#ad9202', label: 'TEST', uri: ''},
        {id: 163434444444444444443776461, color: '#ad9202', label: 'TEST', uri: ''},
        {id: 16346666666666663443776461, color: '#ad9202', label: 'TEST', uri: ''},
        {id: 555555555555555, color: '#ad9202', label: 'TEST', uri: ''},
        {id: 777777777777777777777777, color: '#ad9202', label: 'TEST', uri: ''},
        {id: 90876549, color: '#ad9202', label: 'TEST', uri: ''},
        {id: 117575767, color: '#ad9202', label: 'TEST', uri: ''}
    ])
    // const [soundButtons, setSoundButtons] = useState([])


    const addSoundButton = (soundBtnLabel, soundUri) => {
        const randId  = Math.floor(Math.random() * 1000000000);
        const randColor  = '#' + Math.floor(Math.random()*16777215).toString(16);
        const newButton = {id: randId, color: randColor, label: soundBtnLabel, uri: soundUri}
        setSoundButtons([...soundButtons, newButton])
        closeModal(false)
    }

    const closeModal = () => {
        setIsModalVisible(false)
    }
    const openModal = () => {
        setIsModalVisible(true)
    }

    return (
        <View style={styles.view}>

            <AddButtonModal isVisible={isModalVisible} onPressAddBtn={addSoundButton} onPressCloseButton={closeModal}/>

            <SoundButtons buttons={soundButtons}/>

            <FloatingActionButton onPress={openModal} label="+"/>

            <StatusBar style="light"/>

        </View>
    );
}

const styles = StyleSheet.create({
    view: {
        flex: 1,
        paddingTop: StatusBar.currentHeight,
        backgroundColor: '#000'
    }
});
