import React, {useState} from 'react';
import {StyleSheet, TouchableOpacity, Text} from 'react-native';
import { Audio } from 'expo-av';

export default function RecordButton({setRecordedUri, allowActions}) {
    const [recordingSound, setRecordingSound] = useState(new Audio.Recording())
    const [isRecording, setIsRecording] = useState(false)

    const startRecording = async () => {
        setIsRecording(true)
        allowActions(false)
        try {
            console.log('Requesting permissions..');
            await Audio.requestPermissionsAsync();
            await Audio.setAudioModeAsync({
                allowsRecordingIOS: true,
                playsInSilentModeIOS: true,
            })
            console.log('Starting recording..');
            const sound = await Audio.Recording.createAsync(
                Audio.RECORDING_OPTIONS_PRESET_HIGH_QUALITY
            )
            setRecordingSound(sound.recording);
            console.log('Recording started');
        } catch (err) {
            setRecordingSound(undefined);
            setIsRecording(false)
            console.error('Failed to start recording', err);
        }
    }

    const stopRecording = async () => {
        console.log('Stopping recording..');
        setRecordingSound(undefined);
        await recordingSound.stopAndUnloadAsync();
        const uri = recordingSound.getURI();
        console.log('Recording stopped and stored')

        setRecordedUri(uri)
        setIsRecording(false)
        allowActions(true)
    }

    const handleRecording = () => {
        if(!isRecording) {
            startRecording()
        } else {
            stopRecording()
        }
    }

    return (
        <TouchableOpacity
            onPress={handleRecording}
            style={[styles.button, isRecording ? styles.btnRecording : styles.btnNotRecording]}
        >
            <Text style={[styles.btnText, isRecording ? styles.btnTextRecording : styles.btnTextNotRecording]}>{isRecording ? 'S' : 'R'}</Text>
        </TouchableOpacity>
    );
}

const styles = StyleSheet.create({
    btnText: {
        fontWeight: "bold",
        color: '#FFFF',
        fontSize: 32
    },
    button: {
        width: 80,
        height: 80,
        alignItems: 'center',
        justifyContent: 'center',
        borderRadius: 50,
        borderWidth: 8,
    },
    btnRecording: {
        borderColor: '#b40000',
        backgroundColor: '#FFF'
    },
    btnNotRecording: {
        borderColor: '#000',
        backgroundColor: '#b40000'
    },
    btnTextRecording: {
        color: '#b40000'
    },
    btnTextNotRecording: {
        color: '#FFF'
    }
});
