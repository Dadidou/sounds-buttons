import React from 'react';
import {StatusBar} from 'expo-status-bar';
import {StyleSheet, TouchableOpacity, Text, View} from 'react-native';

export default function SoundButton({color, label, onPress}) {

    return (
        <TouchableOpacity
            onPress={onPress}
            style={[styles.button, {backgroundColor: color}]}
        >
            <Text style={styles.btnLabel}>{label}</Text>
        </TouchableOpacity>
    );
}

const styles = StyleSheet.create({
    button: {
        width: 'auto',
        minWidth: 60,
        height: 60,
        borderRadius: 15,
        alignItems: "center",
        padding: 20,
        marginHorizontal: 6,
        marginVertical: 10
    },
    btnLabel: {
        fontWeight: "bold",
        color: '#fff',
        fontSize: 14
    }
});
