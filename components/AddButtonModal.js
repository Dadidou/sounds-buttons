import {Modal, StyleSheet, Text, TextInput, TouchableOpacity, View} from 'react-native';
import React, {useEffect, useState} from "react";
import RecordButton from "./RecordButton";

export default function AddButtonModal({isVisible, onPressAddBtn, onPressCloseButton}) {
    const [soundBtnLabel, setSoundBtnLabel] = useState('')
    const [soundUri, setSoundUri] = useState('')
    const [areActionsAllowed, setAreActionsAllowed] = useState(true)

    useEffect(() => {
        if (!isVisible) {
            setSoundUri('')
            setSoundBtnLabel('')
        }
    }, [isVisible])

    const allowActions = isAllowing => {
        setAreActionsAllowed(isAllowing)
    }

    return (
        <Modal
            animationType="slide"
            transparent={true}
            visible={isVisible}
        >
            <View style={styles.centeredView}>
                <View style={styles.modalView}>
                    <RecordButton setRecordedUri={(uri) => {setSoundUri(uri)}} allowActions={(isAllowing) => allowActions(isAllowing)}/>

                    <TextInput style={styles.input} onChangeText={setSoundBtnLabel} placeholder="Your button's name" value={soundBtnLabel} maxLength={25} editable={areActionsAllowed}/>

                    <View style={styles.actionsView}>
                        <TouchableOpacity style={[styles.actionBtn, (!soundUri || !soundBtnLabel || !areActionsAllowed) && styles.actionBtnDisabled]} disabled={!soundUri || !soundBtnLabel || !areActionsAllowed} onPress={() => onPressAddBtn(soundBtnLabel, soundUri)}>
                            <Text style={styles.actionBtnText}>Add</Text>
                        </TouchableOpacity>
                        <TouchableOpacity style={[styles.actionBtn, !areActionsAllowed && styles.actionBtnDisabled]} disabled={!areActionsAllowed} onPress={() => onPressCloseButton()}>
                            <Text style={styles.actionBtnText}>Cancel</Text>
                        </TouchableOpacity>
                    </View>
                </View>
            </View>
        </Modal>
    );
}

const styles = StyleSheet.create({
    centeredView: {
        flex: 1,
        justifyContent: "center",
        alignItems: "center"
    },
    modalView: {
        // flex: 0.35,
        justifyContent: 'space-between',
        width: 250,
        height: 250,
        margin: 20,
        backgroundColor: "white",
        borderRadius: 15,
        padding: 35,
        alignItems: "center",
        shadowColor: "#000",
        shadowOffset: {
            width: 0,
            height: 2
        },
        shadowOpacity: 0.25,
        shadowRadius: 4,
        elevation: 5
    },
    actionsView: {
        flexDirection: 'row',
        height: 40
    },
    input: {
      fontSize : 14
    },
    actionBtn: {
        borderRadius: 15,
        height: 40,
        width: 80,
        backgroundColor: '#aec6fc',
        alignItems: 'center',
        justifyContent: 'center',
        marginHorizontal: 10
    },
    actionBtnText: {
        fontWeight: "bold",
        color: '#000',
        fontSize: 14
    },
    actionBtnDisabled: {
        backgroundColor: '#d2d2d2',
    }
});


