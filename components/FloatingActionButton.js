import React from 'react';
import {StatusBar} from 'expo-status-bar';
import {StyleSheet, TouchableOpacity, Text} from 'react-native';

export default function FloatingActionButton({onPress, label}) {

    return (
        <TouchableOpacity
            onPress={onPress}
            style={styles.button}
        >
            <Text style={styles.btnText}>{label}</Text>
        </TouchableOpacity>
    );
}

const styles = StyleSheet.create({
    btnText: {
        fontWeight: "bold",
        color: '#000',
        fontSize: 32
    },
    button: {
        position: 'absolute',
        width: 80,
        height: 80,
        backgroundColor: '#FFF',
        alignItems: 'center',
        justifyContent: 'center',
        right: 30,
        bottom: 30,
        borderRadius: 50
    }
});